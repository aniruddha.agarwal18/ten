# README.md

## Overview

This project is a simple web scraper that fetches, parses, and tokenizes the content of a webpage. The main objective is to convert the HTML content of a webpage into a list of tokens (words) to be used for further processing or analysis by LLM.

## Language and Frameworks

The project is written in Python, a high-level, interpreted programming language that is easy to learn and includes a comprehensive standard library. Python is widely used for web scraping due to its simplicity and the availability of powerful libraries.

The following Python libraries are used in this project:

- `requests`: A simple, yet powerful, HTTP library for sending HTTP requests. It abstracts the complexities of making requests behind a beautiful, simple API, allowing you to send HTTP/1.1 requests.

- `BeautifulSoup`: A library for pulling data out of HTML and XML files. It provides Pythonic idioms for iterating, searching, and modifying the parse tree.

- `nltk`: The Natural Language Toolkit (NLTK) is a platform used for building Python programs that work with human language data for applying in statistical natural language processing (NLP). It provides easy-to-use interfaces to over 50 corpora and lexical resources.

## Design Choices

The project is structured into several functions, each with a specific task, following the Single Responsibility Principle. This makes the code easier to read, test, and maintain.

- `fetch_html(url)`: This function fetches the HTML content of a given URL using the `requests` library. If the request fails, it raises an exception and terminates the program.

- `parse_html(html)`: This function parses the fetched HTML content into a BeautifulSoup object that can be searched and manipulated using BeautifulSoup's methods. It makes use of lxml parser as it is faster than the html parser that BeautifulSoup uses by default. 

- `clean_soup(soup)`: This function cleans the parsed HTML by renaming certain tags, removing unnecessary attributes, and decomposing empty and unwanted tags. The purpose of this function is to simplify the HTML structure and remove noise. This simplification of HTML is needed simplification is needed for a few reasons:
1. Reduce Complexity: HTML has a lot of different tags, each with its own semantics and attributes. Simplifying these tags can make it easier for the language model to understand the structure of the webpage.
2. Improve Generalization: By simplifying tags, we can help the model generalize better. For example, whether a heading is an h1, h2, or h3 might not matter as much as the fact that it’s a heading. By simplifying all heading tags to a single ‘header’ tag, we help the model understand that these are all similar elements.
3. Reduce Dimensionality: Each unique tag is a different dimension in the input data. By reducing the number of unique tags, we reduce the dimensionality of the data, which can make the model’s job easier.

- `flatten_html(soup)`: This function flattens the cleaned HTML into a list of strings. It extracts the text content of each tag and the 'alt' attribute of 'img' tags.

- `tokenize_document(document)`: This function tokenizes the flattened HTML content into a list of words using the `nltk` library. This is the final output of the program.

The main function, `main()`, orchestrates the execution of these functions.

The decision to output data in a tokenized form was made due to the inherent advantages of this format. Tokenization provides a granular level of detail by breaking down the text into individual words or tokens, thereby preserving the sequence of the text. This sequential information is crucial in many Natural Language Processing (NLP) tasks such as machine translation and text summarization, where the order of words significantly impacts the semantic interpretation of the text. Thus, the choice of tokenized output aligns with the objective of maintaining the integrity of the original text's meaning while facilitating further linguistic analysis.

## Conclusion

The choices made were driven by the objective of simplicity and efficiency. Python, with its powerful libraries and easy-to-read syntax, was a natural choice for this task. The modular design of the code ensures that each function can be tested and modified independently, making the project easy to maintain and extend.