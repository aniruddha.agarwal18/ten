import requests
from bs4 import BeautifulSoup
from nltk.tokenize import word_tokenize
import nltk
nltk.download('punkt')  # Download the Punkt tokenizer

# Fetches the HTML content of a given URL
def fetch_html(url):
    try:
        response = requests.get(url)  # Send a GET request to the URL
        response.raise_for_status()  # Raise an exception if the request failed
        return response.text  # Return the text content of the response
    except requests.exceptions.RequestException as e:
        print(f"Error occurred while fetching the URL: {e}")
        exit(1)

# Parses the fetched HTML content into a BeautifulSoup object
def parse_html(html):
    try:
        return BeautifulSoup(html, 'lxml')  # Parse the HTML using lxml parser
    except Exception as e:
        print(f"Error occurred while parsing the HTML: {e}")
        exit(1)

# Cleans the parsed HTML by renaming certain tags, removing unnecessary attributes, and decomposing empty and unwanted tags
def clean_soup(soup):
    tag_name_mapping = {
        'textarea': 'i', 'input': 'i', 'h1': 'header', 'h2': 'header', 'h3': 'header',
        'h4': 'header', 'h5': 'header', 'h6': 'header', 'div': 'container', 'span': 'container',
        'p': 'text', 'a': 'link', 'img': 'media', 'video': 'media', 'audio': 'media',
        'ul': 'container', 'li': 'container',
    }
    attrs_to_remove = set(['class', 'id', 'style', 'onclick', 'onload', 'target', 'rel'])

    for tag in soup.find_all(True):  # Iterate over all tags in the soup
        if 'role' in tag.attrs:  # If the tag has a 'role' attribute
            tag.name = tag['role']  # Rename the tag to its 'role' value
        for attr in attrs_to_remove:  # Iterate over the attributes to remove
            if attr in tag.attrs:  # If the tag has this attribute
                del tag[attr]  # Remove the attribute
        if tag.name in tag_name_mapping:  # If the tag's name is in the mapping
            tag.name = tag_name_mapping[tag.name]  # Rename the tag
        if tag.name == 'container' and not tag.contents:  # If the tag is an empty 'container'
            tag.decompose()  # Remove the tag

    for tag in soup(['script', 'style', 'link', 'svg', 'meta']):  # Iterate over unwanted tags
        tag.decompose()  # Remove the tag

    while True:
        empty_tags = soup.find_all(lambda tag: tag.name in ['container', 'template'] and not (tag.contents or tag.contents == "")) 
        if not empty_tags:  # If there are no more empty tags
            break
        for tag in empty_tags:  # Iterate over the empty tags
            tag.decompose()  # Remove the tag

    return soup  # Return the cleaned soup

# Flattens the cleaned HTML into a list of strings
def flatten_html(soup):
    flattened_html = []
    for tag in soup.find_all(True):  # Iterate over all tags in the soup
        if tag.attrs or tag.string:  # If the tag has attributes or a string content
            flattened_html.append(tag.get_text().strip())  # Append the tag's text content to the list
        if tag.name == 'img' and 'alt' in tag.attrs:  # If the tag is an 'img' with an 'alt' attribute
            flattened_html.append(tag['alt'])  # Append the 'alt' attribute to the list
    return [line for line in flattened_html if line]  # Return the list, excluding empty strings

# Tokenizes the flattened HTML content into a list of words
def tokenize_document(document):
    return word_tokenize(document)  # Use NLTK's word tokenizer

# Main function that orchestrates the execution of the other functions
def main():
    url = "https://github.com/kaavee315"
    html = fetch_html(url)  # Fetch the HTML content of the URL
    soup = parse_html(html)  # Parse the HTML content
    cleaned_soup = clean_soup(soup)  # Clean the parsed HTML
    flattened_html = flatten_html(cleaned_soup)  # Flatten the cleaned HTML
    document = ' '.join(flattened_html)  # Join the flattened HTML into a single string
    tokens = tokenize_document(document)  # Tokenize the document

if __name__ == "__main__":
    main()  # Run the main function if the script is run directly